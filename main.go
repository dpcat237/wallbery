package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"time"
)

const (
	cmdStr          = "'file://%s'"
	minRand         = 0
	logsPath        = "/home/denys/apps/wallbery/logs.txt"
	picturesFolders = "/home/denys/Dropbox/Mobile/picture/wallpaper/views/"
	timePeriod      = 1 * time.Hour
)

type Manager struct {
	pictures []string
	total    int
}

func main() {
	rand.Seed(time.Now().UnixNano())
	var mng Manager
	if err := mng.loadPictures(); err != nil {
		log.Panicln(err)
	}

	doEvery(timePeriod, mng.setNextBackground)
}

func doEvery(d time.Duration, f func(time.Time)) {
	for x := range time.Tick(d) {
		f(x)
	}
}

func (mng *Manager) loadPictures() error {
	files, err := ioutil.ReadDir(picturesFolders)
	if err != nil {
		return err
	}

	var pcs []string
	for _, file := range files {
		pcs = append(pcs, picturesFolders+file.Name())
	}
	mng.pictures = pcs
	mng.total = len(mng.pictures)
	return nil
}

func (mng Manager) logPicture(pic string) {
	lgg := fmt.Sprintf("%s %s", time.Now().Format("2006.01.02 15:04:05"), pic)
	logFl, err := os.OpenFile(logsPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Panicln(err)
	}
	defer logFl.Close()
	if _, err := logFl.WriteString(lgg + "\n"); err != nil {
		log.Panicln(err)
	}
}

func (mng Manager) randPicture() string {
	picIdx := rand.Intn((mng.total-1)-minRand+1) + minRand
	return mng.pictures[picIdx]
}

func (mng Manager) setNextBackground(_ time.Time) {
	pic := mng.randPicture()
	mng.logPicture(pic)
	cmd := exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri-dark", fmt.Sprintf(cmdStr, pic))
	if err := cmd.Run(); err != nil {
		log.Panicln(err)
	}
}
